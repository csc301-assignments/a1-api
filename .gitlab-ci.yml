image: docker:19.03.15
services:
  - name: docker:19.03.15-dind

variables:
  REPO_NAME: gitlab.com/csc301-assignments/a1-api

before_script:
  - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
  - cd $GOPATH/src/$REPO_NAME

  # Creating .env
  - cat $ENV_DEV > .env
  - cat $ENV_DEV > test/.env

stages:
  - build
  - test
  - release
  - deploy
  - production

build:
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --pull -t $CI_REGISTRY_IMAGE .
    - docker push $CI_REGISTRY_IMAGE

test:
  image: golang:latest
  stage: test
  script:
    - go fmt $(go list ./... | grep -v /vendor/)
    - go vet $(go list ./... | grep -v /vendor/)
    - go test -race $(go list ./... | grep -v /vendor/)

release:
  stage: release
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE
    - docker tag $CI_REGISTRY_IMAGE $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE

deploy_dev:
  image: kroniak/ssh-client
  stage: deploy
  variables:
    SSH_ARGS: -o StrictHostKeyChecking=no
    PORT: 8080
    CONTAINER_NAME: a1-api-dev
  environment:
    name: dev
    url: http://34.130.252.55:8080/
  script:
    - chmod 400 $GCE_SSH_KEY
    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"

    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker stop $CONTAINER_NAME || true && sudo docker rm $CONTAINER_NAME || true"

    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker rmi $CI_REGISTRY_IMAGE:latest || true"

    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker pull $CI_REGISTRY_IMAGE"

    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker run -p $PORT:$PORT -d --name $CONTAINER_NAME $CI_REGISTRY_IMAGE ./a1-api"

deploy_prod:
  stage: production
  variables:
    SSH_ARGS: -o StrictHostKeyChecking=no
    PORT: 8000
    CONTAINER_NAME: a1-api-prod
    PROD_IMAGE: $DOCKER_HUB_USER/csc301-a1-api
  environment:
    name: prod
    url: http://34.130.252.55:8000/
  rules:
    - when: manual
  script:
    # changing environment to prod
    - cat $ENV_PROD > .env
    - chmod 400 $GCE_SSH_KEY

    # building production image
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - echo $DOCKER_HUB_PASSWORD | docker login -u $DOCKER_HUB_USER --password-stdin
    - docker pull $CI_REGISTRY_IMAGE
    - docker build --pull -t $PROD_IMAGE:latest .
    - docker push $PROD_IMAGE:latest

    # logging in
    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "echo $DOCKER_HUB_PASSWORD | sudo docker login -u $DOCKER_HUB_USER --password-stdin"

    # stopping old container and removing old image
    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker stop $CONTAINER_NAME || true && sudo docker rm $CONTAINER_NAME || true"
    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker rmi $PROD_IMAGE:latest || true"

    # pulling new image and running container
    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker pull $PROD_IMAGE"

    - ssh $SSH_ARGS -i $GCE_SSH_KEY $GCE_SSH_USERNAME@$SERVER_IP \
      "sudo docker run -p $PORT:$PORT -d --name $CONTAINER_NAME $PROD_IMAGE ./a1-api"
