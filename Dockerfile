FROM golang:latest
WORKDIR /app
COPY . .
RUN go build -race -o a1-api
EXPOSE 8000 8080
# CMD ./a1-api
