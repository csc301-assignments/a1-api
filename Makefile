-include .env

# use to update when make changes to GraphQL query
generate_gql:
	go run github.com/99designs/gqlgen generate

# Need to add database inside mysql repl
setup_local_db: build_migrate_bin
	- docker pull mysql
	- docker stop $(LOCAL_DB_CONTAINER_NAME) || true
	- docker rm $(LOCAL_DB_CONTAINER_NAME) || true
	- docker run -p $(DB_PORT):$(DB_PORT) --name $(LOCAL_DB_CONTAINER_NAME) \
	-e MYSQL_ROOT_PASSWORD=$(DB_PASS) \
	-e MYSQL_DATABASE=$(DB_NAME) -d mysql:latest

restart_local_db:
	docker restart $(LOCAL_DB_CONTAINER_NAME)

empty_local_db:
	- docker stop mysql
	- docker rm -v mysql