# CSC301 Assignment 1 Group 69

By **Supanat Wangsutthitham** and **Eric Lin**

# Production
The application is deployed and available on http://34.130.252.55:3000/.

# Installation
In order to run and test the application locally, you need to clone the following repositories:

1) https://gitlab.com/csc301-assignments/a1-api
2) https://gitlab.com/csc301-assignments/a1-ui

In order for you to clone them, you would need the tokens with their username:

- **For API**: `xM5Nx5TZ5s5Vy5pFXEke`
- **For UI**: `t9KffRffc6kg_7ee94WR`

Both tokens share the same username: `csc301-a1-69`
The command to clone the repositories is

`git clone http://<username>:<token>@gitlab.com/<owner>/<repo>.git`
  
Note that the owner here is `csc301-assignments` and the repo is either `a1-api` or `a1-ui`.

After you have cloned the two repositories, navigate to the API repository directory to create .env file to make it run properly. In the file, you must include

| variable | description |
| --- | --- |
| `PORT`    |   the port you wish to host your API.                                   |
| `DB_PORT` |   the port in which the database is listening                           |
| `DB_NAME` |   the database name                                                     |
| `DB_USER` |   the database username (use root when run locally)                     |
| `DB_PASS` |   the database password                                                 |
| `DB_HOST` |   the ip of the database (use localhost when run locally)               |
| `LOCAL_DB_CONTAINER_NAME` | (optional) the name of the container of your database   |

Also, [Docker](https://docs.docker.com/get-docker/) is recommended to run the local database (with the provided Makefile).

## Starting up the backend
Run `make generate_gql` to generate GraphQL related files.

Run `make setup_local_db` to create a container for our database.

Run `docker exec -it mysql bash` to get inside mysql repl.

Then, login to mysql with `mysql -u <DB_USER> -p`. You will be asked for a password, which is `DB_PASS`.

Now, go to your database via `use <DB_NAME>;`, and create a table with

`CREATE TABLE IF NOT EXISTS Items(
    ID INT NOT NULL UNIQUE AUTO_INCREMENT,
    Name VARCHAR (255) NOT NULL,
    Price DECIMAL (6, 2) NOT NULL,
    PRIMARY KEY (ID)
);`

This will create a table in our database. You can now run the backend with `go run server.go` on the root directory (of the API repo).

## Starting the site
Then, go to the UI repo. In the folder `src/functions/getResponse.ts`, uncomment line 5 and comment line 4. Note that you might have to change the port from `8000` to `PORT` to make it run properly.

You can now start the site using `yarn start`, which you should be able to access the site shortly afterward on `http://localhost:3000`.
