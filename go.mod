module gitlab.com/csc301-assignments/assignment-1/a1-api

replace gitlab.com/csc301-assignments/assignment-1/a1-api => gitlab.com/csc301-assignments/a1-api v1.0.0

go 1.17

require (
	github.com/99designs/gqlgen v0.14.0
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/chi/v5 v5.0.4
	github.com/go-sql-driver/mysql v1.6.0
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.6.0
	github.com/vektah/gqlparser/v2 v2.2.0
)

require (
	github.com/agnivade/levenshtein v1.1.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/golang-lru v0.5.1 // indirect
	github.com/matryer/moq v0.0.0-20200106131100-75d0ddfc0007 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/urfave/cli/v2 v2.1.1 // indirect
	github.com/vektah/dataloaden v0.2.1-0.20190515034641-a19b9a6e7c9e // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/tools v0.1.5 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
