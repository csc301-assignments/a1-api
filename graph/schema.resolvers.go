package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/csc301-assignments/assignment-1/a1-api/graph/generated"
	"gitlab.com/csc301-assignments/assignment-1/a1-api/graph/model"
	"gitlab.com/csc301-assignments/assignment-1/a1-api/internal/items"
)

func (r *mutationResolver) CreateItem(ctx context.Context, input model.NewItem) (*model.Item, error) {
	item := items.Item{
		Name:  input.Name,
		Price: input.Price,
	}
	itemId := item.Save()

	return &model.Item{ID: strconv.FormatInt(itemId, 10), Name: item.Name, Price: item.Price}, nil
}

func (r *mutationResolver) DeleteItem(ctx context.Context, input string) (string, error) {
	idInt, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}

	err = items.DeleteById(int64(idInt))
	if err != nil {
		return "", nil
	}

	result := fmt.Sprintf("Successfully deleted item with ID %d", idInt)

	return result, err
}

func (r *queryResolver) Item(ctx context.Context, id string) (*model.Item, error) {
	idInt, err := strconv.Atoi(id)
	if err != nil {
		log.Fatal(err)
	}

	item, err := items.GetById(int64(idInt))
	if err != nil {
		return nil, err
	}

	return &model.Item{ID: item.ID, Name: item.Name, Price: item.Price}, nil
}

func (r *queryResolver) Items(ctx context.Context) ([]*model.Item, error) {
	returnItems := []*model.Item{}
	dbItems := items.GetAll()
	for _, item := range dbItems {
		returnItems = append(returnItems, &model.Item{ID: item.ID, Name: item.Name, Price: item.Price})
	}

	return returnItems, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
