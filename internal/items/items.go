package items

import (
	// Built-ins
	"errors"
	"fmt"
	"log"

	// Local imports
	db "gitlab.com/csc301-assignments/assignment-1/a1-api/internal/db/mysql"
)

type Item struct {
	ID    string  `json:"id"`
	Name  string  `json:"name"`
	Price float64 `json:"price"`
}

func (item Item) Save() int64 {
	stmt, err := db.Db.Prepare("INSERT INTO Items(Name, Price) VALUES(?,?)")
	if err != nil {
		log.Fatal(err)
	}

	res, err := stmt.Exec(item.Name, item.Price)
	if err != nil {
		log.Fatal(err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Fatal("Error: ", err.Error())
	}

	log.Println("Item inserted")

	return id
}

func GetById(id int64) (Item, error) {
	sql_query := fmt.Sprintf("SELECT Id, Name, Price FROM Items WHERE id = %d LIMIT 1", id)
	stmt, err := db.Db.Prepare(sql_query)
	if err != nil {
		return Item{}, err
	}
	defer stmt.Close()

	row, err := stmt.Query()
	if err != nil {
		return Item{}, err
	}
	defer row.Close()

	item := Item{}
	row.Next()
	if err := row.Scan(&item.ID, &item.Name, &item.Price); err != nil {
		return Item{}, err
	}

	return item, err
}

func GetAll() []Item {
	stmt, err := db.Db.Prepare("SELECT Id, Name, Price FROM Items")
	if err != nil {
		log.Fatal(err)
	}

	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()
	items := []Item{}

	for rows.Next() {
		item := Item{}
		err := rows.Scan(&item.ID, &item.Name, &item.Price)
		if err != nil {
			log.Fatal(err)
		}
		items = append(items, item)
	}

	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}

	return items
}

func DeleteById(id int64) error {
	sql_query := fmt.Sprintf("DELETE FROM Items WHERE Id=%d", id)
	stmt, err := db.Db.Prepare(sql_query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	res, err := stmt.Exec()
	if err != nil {
		return errors.New("Error, failed to execute SQL statement")
	}

	count, err := res.RowsAffected()
	if err != nil {
		return errors.New("Error, failed during getting number of rows deleted")
	} else if count != 1 {
		return errors.New("Error, failed to delete the item")
	}

	log.Println("Item deleted")

	return nil
}
