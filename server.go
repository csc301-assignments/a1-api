package main

import (
	// Built-ins
	"log"
	"net/http"
	"os"

	// External libs
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/go-chi/chi"
	"github.com/joho/godotenv"
	"github.com/rs/cors"

	// Local imports
	"gitlab.com/csc301-assignments/assignment-1/a1-api/graph"
	"gitlab.com/csc301-assignments/assignment-1/a1-api/graph/generated"
	db "gitlab.com/csc301-assignments/assignment-1/a1-api/internal/db/mysql"
	"gitlab.com/csc301-assignments/assignment-1/a1-api/internal/playground"
)

const defaultPort = "8080"

func main() {
	// Load environment variables
	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}

	// Get port from env
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	// Initialize Chi Router
	router := chi.NewRouter()

	// Allowing CORS
	router.Use(cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:3000", "http://34.130.252.55:3000"},
		AllowedHeaders: []string{"Access-Control-Allow-Origin", "Content-Type"},
		// Debug:          true,
	}).Handler)

	// Initizalize database and migrate
	db.InitDb()

	srv := handler.NewDefaultServer(
		generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
