package test

import (
	// Built-ins

	"log"
	"testing"

	// External libs
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"

	// Local imports
	"gitlab.com/csc301-assignments/assignment-1/a1-api/graph"
	"gitlab.com/csc301-assignments/assignment-1/a1-api/graph/generated"
	db "gitlab.com/csc301-assignments/assignment-1/a1-api/internal/db/mysql"
	"gitlab.com/csc301-assignments/assignment-1/a1-api/internal/items"
)

func ConnectToDb() {
	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}

	// Initialize Chi Router
	router := chi.NewRouter()

	// Initizalize database and migrate
	db.InitDb()

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	// Creating endpoints
	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", srv)

	log.Println("Ready to listen for requests")
}

/*  ========== Example of Making Request via Golang ==========

// Creating URL
url := fmt.Sprintf("http://localhost:%s/query", port)
body := strings.NewReader("{\"query\": \"{item(id: 1) {id, name, price}}\"}")
// body := strings.NewReader("{\"query\": \"{items {name}}\"}")
req, err := http.NewRequest(http.MethodPost, url, body)
if err != nil {
	log.Fatal("Ah sheet: ", err)
}

// Setting request headers
req.Header.Set("Content-Type", "application/json")
req.Header.Set("Accept", "application/json")

// Sending request
res := httptest.NewRecorder()
srv.ServeHTTP(res, req)

// Testing response
fmt.Println("Response Body: ", res.Body.String())
if strings.Contains(res.Body.String(), "errors") {
	fmt.Errorf("Error")
}

=========================================================== */

func TestItemsGetAll(t *testing.T) {
	// TODO
}

// TODO: Find a way to separate GetById and Save
// func TestItemsGetById(t *testing.T) {
// 	ConnectToDb()
// 	expected := items.Item{
// 		Name:  "Test",
// 		Price: 301.301,
// 	}
// 	itemId := expected.Save()

// 	item, err := items.GetById(itemId)
// 	if err != nil {
// 		t.Errorf("Error while getting the item")
// 	}

// 	if item.Name != expected.Name {
// 		t.Errorf("Test failed, expected: %s, got: %s", expected.Name, item.Name)
// 	}
// }

func TestItemsGetByIdAndSave(t *testing.T) {
	ConnectToDb()

	expected := items.Item{
		Name:  "CSC301 A1 Group 69",
		Price: 301.69,
	}
	itemId := expected.Save()

	// Receiving item using GetById
	result, err := items.GetById(itemId)
	if err != nil {
		t.Errorf("Error while getting the item")
	}

	// Check Name
	if expected.Name != result.Name {
		t.Errorf("Test failed, expected %s, got %s", expected.Name, result.Name)
	}

	// Check Price
	if expected.Price != result.Price {
		t.Errorf("Test failed, expected %f, got %f", expected.Price, result.Price)
	}

	// Clean up
	err = items.DeleteById(itemId)
	if err != nil {
		t.Errorf("Error while removing the item")
	}
}

func TestItemsDeleteById(t *testing.T) {
	ConnectToDb()

	// Inserting new item
	item := items.Item{
		Name:  "Inserting Item",
		Price: 99.99,
	}
	itemId := item.Save()

	// Deleteing new item
	err := items.DeleteById(itemId)
	if err != nil {
		t.Errorf("Error while deleteing the item")
	}

	// Expecting Error
	item, err = items.GetById(itemId)
	if err == nil {
		t.Errorf("Test failed, managed to successfully get the item")
	}

}
